LIFAP5 - projet 2019-2020 : gestionnaire de QCM
===============================================

## Table des matières

- [etudiants](#etudiants)
- [contenu du dossier](#contenu-du-dossier)
- [partie 1](#partie-1)
- [partie 2](#partie-2)
- [remarques additionnelles](#remarques-additionnelles)

## Etudiants

* ISMAIL Lina 11802713
* MHENNI Hassen 11802946

## Contenu du dossier

- `index.html`: page principale
- `css/`: style (materialize)
- `js/`: contient les differents fonctions (vues.js: affichage; modele: récupère données; app.js: envoie données)

## Partie 1

* [X] **Modifier l'utilisateur connecté** : il faut pour cela remplir la champ `xApiKey` de l'objet `state` déclaré dans `js/modeles.js`. Comprendre le fonctionnement permettant le mise à jour de l'état (dans `js/modeles.js`) et la modification du comportement du bouton "utilisateur" (dans `js/vues.js`). Ensuite, il faut permettre de choisir l'utilisateur avec lequel on se connecte et se délogguer le cas échéant.
* [X] **Afficher les questions et les propositions d'un quiz** : lorsque l'on clique sur un quiz, la fonction `clickQuiz` (définie dans `js/vues.js`) est appelée. Elle appelle `renderCurrentQuizz` qui va changer l'affichage du div HTML `id-all-quizzes-main`. Modifier ces fonctions de façon à afficher les questions (et leurs propositions de réponses) du quiz au lieu de "Ici les détails pour le quiz _xxyyzz_".
* [X] **Répondre à un quiz**: modifier l'affichage précédent de façon à pouvoir répondre au quiz, c'est-à-dire pouvoir cocher la réponse choisie à chaque question, puis cliquer sur un bouton "Répondre" qui enverra les réponses au serveur.
* [X] **Afficher les quiz de l'utilisateur connecté et des réponses déjà données** : reprendre la fonctionnalité d'affichage de tous les quiz et l'adapter pour afficher les quiz de l'utilisateur connecté dans l'onglet "MES QUIZ". Similairement, remplir l'onglet "MES REPONSES" pour afficher les quiz auxquels l'utilisateur connecté a répondu
* [X] **Créer un quiz pour l'utilisateur connecté** : ajouter un formulaire permettant de saisir les informations d'un nouveau quiz dans l'onglet "MES QUIZ". Ajouter un bouton "Créer" qu déclenchera l'ajout du quiz sur le serveur et le rafraîchissement de la liste des quiz. Permettre d'ajouter aux quiz de l'utilisateur connecté un formulaire d'ajout de question. Ce formulaire permettra de saisir les propositions possibles pour la question. Sa validation déclenchera l'ajout de la question sur le serveur.


## Partie 2

* [X] **Catégorie modifications de quiz et de leurs questions** : Mettre à jour un quiz : changer la description et le titre d'un quiz. Ajouter un bouton "Modifier" qui va faire apparaître un formulaire de modification. Gérer ce formulaire pour mettre à jour les données sur le serveur. Mettre à jour l'énoncé une question: changer la phrase (sentence) d'énoncé et permettre de modifier une proposition: ajouter par exemple un bouton de suppression à côté de chaque question (NB, le serveur ne propose pas de route permettant de manipuler les propositions, tout passe par l'API des questions)

* [X] **Catégorie affichage des quiz** : Permettre de choisir le nombre de résultats, le critère de tri des quiz ainsi que l'ordre (croissant ou décroissant) de l'affichage. Gérer la pagination pour afficher la liste des pages existantes avec le composant pagination de Materialize.

* [X] **Catégorie simplification IHM, suppression de MES QUIZ** : Supprimer l'onglet "MES QUIZ" et intégrer ses fonctionnalités à l' onglet "TOUS LES QUIZ" en faisant en sorte que si le quiz affiché appartient à l'utilisateur connecté, les fonctionnalités de création (et modifications le cas échéant) sont accessibles.

* [X] **Catégorie formulaire de recherche** : Le formulaire de recherche en haut de la page permet de recherche en texte plein sur tous les champs de texte des quiz, des questions et des propositions. Changer le comportement de l'onglet "TOUS LES QUIZ" pour mettre en surbrillance les éléments retournés par le résultat de la recherche.

## Remarques additionnelles

                                                                    RAS                                              