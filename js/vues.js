/* global state QQuizzes */

// ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// HTML : generation liste des quizz
// ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// génération d'une liste de quizzes avec deux boutons en bas avec la pagination
const htmlQuizzesList = (quizzes, curr, total) => {
	console.debug(`@htmlQuizzesList(.., ${curr}, ${total})`);

	//Si l'ID du quizz appartient à la liste d'id des quizz qui sont rechercher alors
	//la couleur de la collection est jaune sinon la collection est bleu
	const quizzesLIst = quizzes.map(
		(q) =>
			`<li class="collection-item ${
				state.researchID.includes(q.quiz_id)
					? "#ffeb3b yellow"
					: "modal-trigger cyan lighten-5"
			} "data-quizzid="${q.quiz_id}">
         <h5>${q.title}</h5>${q.description} <a class="chip">
         ${q.owner_id}</a></li>`
	);

	// le bouton "<" pour revenir à la page précédente, ou rien si c'est la première page
	// on fixe une donnée data-page pour savoir où aller via JS via element.dataset.page
	const prevBtn =
		curr !== 1
			? `<button id="id-prev-quizzes" data-page="${
					curr - 1
			  }" class="btn"><i class="material-icons">navigate_before</i></button>`
			: "";

	// le bouton ">" pour aller à la page suivante, ou rien si c'est la première page
	const nextBtn =
		curr !== total
			? `<button id="id-next-quizzes" data-page="${
					curr + 1
			  }  " class="btn"><i class="material-icons">navigate_next</i></button>`
			: "";

	// La liste complète et les deux boutons en bas avec la pagination
	const html = `
  <ul class="collection">
    ${quizzesLIst.join("")}
  </ul>
  <div class="row">      
    ${pagination(
			prevBtn,
			state.quizzes.nbPages,
			state.quizzes.currentPage,
			nextBtn
		)}
  </div>
  `;
	return html;
};

// ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// HTML : generation details d'un quizz
// ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// génération de la description d'un quizz (Titre, date de création, propriétaire)
const htmlDescription = (infoQuizz) => {
	return ` <div align=center><h4>${infoQuizz.title}</h4>
  <p>Crée à ${new Date(infoQuizz.created_at).toLocaleTimeString()} par ${
		infoQuizz.owner_id
	}</p> 
  <h7>${infoQuizz.description}</h7>
  </div>`;
};

// génération de la liste des questions avec leurs différentes propositions
const htmlQuestion = (questions) =>
	questions.map(
		(ques) =>
			` <p>${ques.sentence}</p> 
      ${htmlPropositions(ques.propositions, ques.question_id).join("")}`
	);

// génération des propositions d'une question
const htmlPropositions = (props, id) =>
	props.map(
		(p) => `<label>
  <input type="radio" name="${id}" value="${p.proposition_id}"><span>${p.content}</span>
</label>`
	);

// indique si le quizz doit être available ou non
const available = (info) => {
	if (info == false) {
		return false;
	} else if (state.user == undefined) return false;
	else return true;
};

// génération les details d'un quizz sous forme de formulaire
// avec un boutons pour valider (dispo/indispo)
const htmlQuestions = (questions, info) => {
	return `<form id="quizzSend">
            ${htmlQuestion(questions).join("")}
            </br></br><div align=center><button class="btn" type="submit"  ${
							available(info) ? "" : "disabled"
						}>
            <span>${available(info) ? "Envoyer" : "indisponble"}</span>
            <i class="material-icons">${available(info) ? "send" : "block"}</i>
            </button></div>
            </form>`;
};

// ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Catégorie affichage des quiz: TRI
// ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// On modifie les informations du tri en fonction de l'évaluation de order
// puis on rappel GetQuizzes avec les nouvelles informations sur le tri
// si on doit modifier les sens (croissant/decroissant)
// on rappel tri recursivement avec un autre indice
const tri = (order) => {
	switch (order) {
		case 1:
			if (state.tri.order != "quiz_id") {
				state.tri.order = "quiz_id";
				getQuizzes();
				break;
			} else tri(6);
			break;
		case 2:
			if (state.tri.order != "owner_id") {
				state.tri.order = "owner_id";
				getQuizzes();
				break;
			} else tri(6);
			break;
		case 3:
			if (state.tri.order != "created_at") {
				state.tri.order = "created_at";
				getQuizzes();
				break;
			} else tri(6);
			break;
		case 4:
			if (state.tri.order != "title") {
				state.tri.order = "title";
				getQuizzes();
				break;
			} else tri(6);
			break;
		case 5:
			state.tri.limit = state.tri.limit * 2;
			getQuizzes();
			break;
		case 6:
			if (state.tri.dir == "asc") {
				state.tri.dir = "desc";
				getQuizzes();
				break;
			} else state.tri.dir = "asc";
			getQuizzes();
			break;
	}
};

// Les boutons de tri: avec des boutons pour: ID, OWNER, DATE, TITLE,
// pour AJOUTER des quizzes dans la liste dans une même page
// pour changer de direction (croissant/decroissant):
// il suffit de recliquer sur les boutons (ID,OWNER,DATE ou TITLE)
const btnTri = () => {
	return `<div align=center>
  <label>Type de tri: ${state.tri.order} ${
		state.tri.dir == "asc" ? " par ordre croissant" : " par ordre décroissant"
	}</label></br>
  <button class="btn" type="submit" onclick="tri(1);">ID<i class="material-icons">import_export</i></button>
  <button class="btn" type="submit" onclick="tri(2);">OWNER<i class="material-icons">import_export</i></button>
  <button class="btn" type="submit" onclick="tri(3);">DATE<i class="material-icons">import_export</i></button>
  <button class="btn" type="submit" onclick="tri(4);">TITLE<i class="material-icons">import_export</i></button>
  <button class="btn" type="submit" onclick="tri(5);">PLUS DE QUIZ</button>
  </div>`;
};

// ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Catégorie affichage des quiz: PAGINATION
// ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// pagination genere pagination de la page selon le nombre de pages possible de la page
const pagination = (btnPrev, nbrPage, pageActu, btnSuiv) => {
	const tabNumPage = Array.from({ length: nbrPage }, (_, k) => k + 1);
	return `<ul class="pagination"><div align="center">
  ${btnPrev}     ${tabNumPage
		.map((num) => lienPage(num, pageActu))
		.join("")}     ${btnSuiv}
  </div></ul>`;
};

// retoune un lien vers la page i de quiz
const lienPage = (i, page) => {
	if (i === page) return `<li class="btn-floating"><a>${i}</a></li>`;
	return `<li class="waves-effect waves-teal btn-flat"><a onclick="getQuizzes(${i})">${i}</a></li>`;
};

// ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// RENDUS : mise en place du HTML dans le DOM et association des événemets: TOUS LES QUIZ
// ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// met la liste HTML dans le DOM et associe les handlers aux événements
function renderQuizzes() {
	console.debug(`@renderQuizzes()`);
	// les éléments à mettre à jour : le conteneur pour la liste des quizz
	const usersElt = document.getElementById("id-all-quizzes-list");

	// on appelle les fonction de génération et on met le HTML produit dans le DOM
	usersElt.innerHTML =
		btnModif() +
		btnTri() +
		htmlQuizzesList(
			state.quizzes.results,
			state.quizzes.currentPage,
			state.quizzes.nbPages
		);

	// les éléments à mettre à jour : les boutons
	const prevBtn = document.getElementById("id-prev-quizzes");
	const nextBtn = document.getElementById("id-next-quizzes");

	// la liste de tous les quizzes individuels
	const quizzes = document.querySelectorAll("#id-all-quizzes-list li");

	function clickBtnPager() {
		getQuizzes(this.dataset.page);
	}

	if (prevBtn) prevBtn.onclick = clickBtnPager;
	if (nextBtn) nextBtn.onclick = clickBtnPager;

	function clickQuiz() {
		console.debug(`@clickQuiz(${this.dataset.quizzid})`);

		// le conteneur principale où on veut afficher le quizze
		const main = document.getElementById("id-all-quizzes-main");

		// on récupère ID dans l'etat global
		if (this.dataset.quizzid != undefined) {
			state.currentQuizz = this.dataset.quizzid;

			// on recupere les donnees du quiz puis
			// on affiche ce quiz dans main
			return getDonneesQuizz(state.currentQuizz).then(() => {
				return renderCurrentQuizz(main);
			});
		}
	}

	// quand on clique sur un quizz, on change sont contenu avant affichage
	quizzes.forEach((q) => {
		q.onclick = clickQuiz;
	});
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// simplification IHM, suppression "MES QUIZ"
// ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Les differentes action que l'user peut faire sur le quiz
const menuAction = `</br></br></br><h4>Que souhaitez-vous faire avec votre quizz?</h4></br></br>
  <button class="btn" type="submit" onclick="visu();">le visualiser 
  <i class="material-icons">remove_red_eye</i></button></br></br>
  <button class="btn" type="submit" onclick="modifQuestion();">modifier une question 
  <i class="material-icons">edit</i></button></br></br>
  <button class="btn" type="submit" onclick="suppQuestion();">supprimer une question
  <i class="material-icons">delete</i></button></br></br>
  <button class="btn" type="submit" onclick="ModifQuizz();">modifier ses caractéristiques
  <i class="material-icons">edit</i></button></br></br>
`;

// affiche un aperçu du quiz
// le bouton renvoie une alerte
const visu = () => {
	const data = state.currentQuizzData;
	const main = document.getElementById("id-all-quizzes-main");
	main.innerHTML =
		retour +
		htmlDescription(data.info) +
		htmlQuestions(data.questions, data.info.open);
	const form = document.forms["quizzSend"];
	form.addEventListener("submit", (e) => {
		e.preventDefault();
		alert("Malheureusement, vous ne pouvez pas répondre à votre propre quiz");
	});
};

// determine si il faut afficher un quiz de l'user ou un autre quiz
// et lance l'affichage en conséquence
//-> formulaire si il appartient à un autre user
//-> menu d'action si il appartient à l'user
function renderCurrentQuizz(
	main = document.getElementById("id-all-quizzes-main")
) {
	console.debug(`@renderCurrentQuizz()`);
	const data = state.currentQuizzData;
	const Id = data.info.quiz_id;
	mainInner(state.UserQuizzesID.includes(Id));

	function mainInner(inclu) {
		getUserQuizzes();
		if (inclu == true) {
			main.innerHTML = menuAction;
		} else {
			main.innerHTML =
				htmlDescription(data.info) +
				htmlQuestions(data.questions, data.info.open);
			const form = document.forms["quizzSend"];
			form.addEventListener("submit", (e) => {
				e.preventDefault();
				const formData = new FormData(form);
				const data = {};
				postAnswer(formData, data);
			});
		}
	}
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// modifications de quiz et de leurs questions
// ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// bouton retour vers menu action (attention pas maj)
const retour = `</br></br><button class="btn" type="submit" onclick="renderCurrentQuizz()";>Retour</button>`;

// Affichage action reediter => modifier une question

const formulaireReediterChoix = (Id, html) => {
	return `<h4>Rééditer le quizz ${Id}</h4>  
<form id="choixSentence">
<div class="input-field col s8">
  <select id="list-Sentence" class="browser-default">
  <option value="" disabled selected>Veuillez selectionner la question que vous souhaitez modifier</option>
  ${html};
  </select>
<div align=center>
  </br></br><button class="btn" type="submit">Suivant<i class="material-icons"></i></button>
</div></div>
</form>`;
};

const formulaireReediter = (Id, idQ, question, reponse, sele) => {
	return `<h4>Rééditer le quizz ${Id}: question ${idQ}</h4> 
<form id="modifQuestion"></br></br>
<div class="input-field col s8">
<span>La question:</span></br>
<input id="Question" type="text" class="validate" value="${question}"></br></br>
<div id="Answers">${reponse}</div></br>
<span>La bonne réponse?</span></br>
<select id="list-BonneRep" class="browser-default">${sele}</select></br></br>
<div align=center><button class="btn" type="submit">Modifier<i class="material-icons"></i></button></div></div>
</form>`;
};
// les option du selecte pour le choix de la question à modifier
const listeQuestion = (question) => {
	return question.map(
		(q) =>
			`<option id="${q.question_id}" value="${q.sentence}">${q.sentence}</option>`
	);
};

// les différents proposition inputs avec comme valeur
// par defaut les anciennes valeur des proposition
const listeReponse = (reponse) => {
	return reponse.map(
		(q) =>
			`<span>La réponse numéro ${q.proposition_id}</span>
  <input id="La reponse-${q.proposition_id}" type="text" class="validate" value="${q.content}"></br>`
	);
};

// les option du selecte pour le choix de la bonne reponse
const listSelect = (reponse) => {
	return reponse.map(
		(q) =>
			`<option id="${q.proposition_id}" value="${q.proposition_id}"> La réponse numéro: ${q.proposition_id}</option>`
	);
};

// lance l'affichage deux formulaire pour le choix de la question
// puis pour la modifcation de la question choisi
// puis à la validation on lance updateQuestion avec les données
// necessaire id de la question et du quiz
// et les donnees de la question
const modifQuestion = () => {
	const data = state.currentQuizzData;
	const main = document.getElementById("id-all-quizzes-main");
	const html = listeQuestion(data.questions);
	const Id = state.currentQuizz;
	main.innerHTML = retour + formulaireReediterChoix(Id, html);
	const form = document.forms["choixSentence"];
	form.addEventListener("submit", (e) => {
		e.preventDefault();
		const upd = (value, id, bool) => {
			return {
				content: value,
				proposition_id: id,
				correct: bool,
			};
		};
		const idsentence =
			document.getElementById("list-Sentence").selectedIndex - 1;
		const reponse = listeReponse(data.questions[idsentence].propositions).join(
			""
		);
		const sele = listSelect(data.questions[idsentence].propositions);
		main.innerHTML =
			retour +
			formulaireReediter(
				Id,
				idsentence,
				data.questions[idsentence].sentence,
				reponse,
				sele
			);
		const form2 = document.forms["modifQuestion"];
		form2.addEventListener("submit", (e) => {
			e.preventDefault();
			const Question = document.getElementById("Question").value;
			const propale = Array.from(
				{
					length: data.questions[idsentence].propositions_number,
				},
				(_, k) =>
					upd(
						document.getElementById("La reponse-" + k).value,
						k,
						idsentence == k
					)
			);
			update = {
				sentence: Question,
				propositions: propale,
			};
			updateQuestion(update, Id, idsentence);
		});
	});
};

// Affichage action suppQuestion => supprimer Question
const formulaireSuppQuestion = (Id, html) => {
	return `<h4>Supprimer une question du quizz ${Id}</h4>  
<form id="choixSentence">
<div class="input-field col s8">
  <select id="list-Sentence" class="browser-default">
  <option value="" disabled selected>Veuillez selectionner la question</option>
  ${html};
  </select>
<div align=center>
  </br></br><button class="btn" type="submit">Supprimer</button>
</div></div>
</form>`;
};

// lance l'affichage d'un formulaire pour le choix de la question à supprimer
// puis à la validation on lance deleteQuestion avec les données necessaire
const suppQuestion = () => {
	const data = state.currentQuizzData;
	const main = document.getElementById("id-all-quizzes-main");
	const html = listeQuestion(data.questions);
	const Id = state.currentQuizz;
	main.innerHTML = retour + formulaireSuppQuestion(Id, html);
	const form = document.forms["choixSentence"];
	form.addEventListener("submit", (e) => {
		e.preventDefault();
		const idsentence =
			document.getElementById("list-Sentence").selectedIndex - 1;
		deleteQuestion(idsentence, Id);
	});
};
// convertie true =>ouvrir et false=>fermer
const dispo2 = (d) => {
	if (d == true) {
		return "ouvert";
	} else return "fermer";
};

// Affichage action ModifQuizz => modifie quiz
const formulaireModif = (info, Id) => {
	return `<h4>Modification du quizz ${Id}</h4>
  <form id="ModificationduQuizz">
  <div class="input-field col s8">
      <select id="listDispo_modif" class="browser-default">
      <option value="" disabled selected>${dispo2(info.open)}</option>
      <option id="1">ouvrir</option>
      <option id="2">fermer</option>
      </select>
    </div>
    <div class="input-field col s8">
      <input id="titre_Quizz_modif" type="text" class="validate" value="${
				info.title
			}">
    </div>
    <div class="input-field col s8">
      <textarea id="description_Quizz_modif" class="materialize-textarea">${
				info.description
			}</textarea>
    </div></br>
    <div class="input-field col s8" align="center"><button class="btn" type="submit">
  <span>Envoyer</span><i class="material-icons">send</i>
  </button></div>
  </form>`;
};

// lance l'affichage d'un formulaire pour la modif d'un quiz
// puis à la validation on lance modifieQuizz on recupere les donnes des champs
// avec les données necessaire pour modif du quiz
const ModifQuizz = () => {
	const data = state.currentQuizzData;
	const Id = state.currentQuizz;
	const main = document.getElementById("id-all-quizzes-main");
	main.innerHTML = retour + formulaireModif(data.info, Id);
	const form = document.forms["ModificationduQuizz"];
	form.addEventListener("submit", (e) => {
		e.preventDefault();
		const idselection = document.getElementById("listDispo_modif")
			.selectedIndex;
		const a = document.getElementById("listDispo_modif").options[idselection];
		const title = document.getElementById("titre_Quizz_modif").value;
		const description = document.getElementById("description_Quizz_modif")
			.value;
		if (a.id == 1) {
			const open = true;
			const d = { title, description, open };
			modifieQuizz(d, Id);
		} else if (a.id == 2) {
			const open = false;
			const d = { title, description, open };
			modifieQuizz(d, Id);
		} else {
			const open = state.currentQuizzData.info.open;
			const d = { title, description, open };
			modifieQuizz(d, Id);
		}
	});
};

// ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Créer un quiz pour l'utilisateur connecté
// ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// les boutons pour la création, edition, et suppresion de quizz
// (dispo/indispo) si l'user est connecté
const btnModif = () => {
	return `<div align=center><label>Action sur les quizz:</label></br>
  <a class="btn-floating btn-large waves-effect waves-light " onclick="AjouterQuizz();" 
  ${
		state.xApiKey == "" ? "disabled" : ""
	}><i class="material-icons">add_box</i></a>
  <a class="btn-floating btn-large waves-effect waves-light "onclick="EditerQuizz();"
  ${
		state.xApiKey == "" ? "disabled" : ""
	}><i class="material-icons">edit</i></a>
  <a class="btn-floating btn-large waves-effect waves-light" onclick="SupprimerQuizz();"
  ${
		state.xApiKey == "" ? "disabled" : ""
	}><i class="material-icons">delete</i></a>
  </div>`;
};

const formulaireCreation = () => {
	return `<h4>Créer un Quizzz</h4>
  <form id="ajouterQuizz">
  <div class="input-field col s8">
      <select id="listDispo" class="browser-default">
      <option value="" disabled selected>${state.user.lastname}, voulez-vous que les utilisateurs aient la possibilité d'y répondre?</option>
      <option id="1">oui</option>
      <option id="2">non</option>
      </select>
    </div>
    <div class="input-field col s8">
      <input id="titre_Quizz" type="text" class="validate">
      <label for="Titre">Nom du quizz</label>
    </div>
    <div class="input-field col s8">
      <textarea id="description_Quizz" class="materialize-textarea"></textarea>
      <label for="description">Description</label>
    </div></br>
    <button class="btn" type="submit">
  <span>Envoyer</span><i class="material-icons">send</i>
  </button>
  </form>`;
};

const AjouterQuizz = () => {
	console.debug(`@ajouterQuizz`);
	const main = document.getElementById("id-all-quizzes-main");
	main.innerHTML = formulaireCreation();
	const form = document.forms["ajouterQuizz"];
	form.addEventListener("submit", (e) => {
		e.preventDefault();
		idselection = document.getElementById("listDispo").selectedIndex;
		const a = document.getElementById("listDispo").options[idselection];
		const title = document.getElementById("titre_Quizz").value;
		const description = document.getElementById("description_Quizz").value;
		const open = a.id == 1;
		const data = { title, description, open };
		postQuizz(data);
	});
};

const formulaireEditer = (html) => {
	return `<h4>Editer un Quizzz</h4>  
  <button class="btn" type="submit" onclick="AddAnswer();">Ajouter une réponse</button>
  <form id="EditQuizz">
    <div class="input-field col s8">
      <select id="list" class="browser-default">
        <option value="" disabled selected>${state.user.lastname}, veuillez selectionner le quizz.</option>
        ${html};
      </select>
      <select id="list-BonneRep" class="browser-default">
      <option value="" disabled selected>Veuillez selectionner la bonne reponse</option>
      </select>
    </div>
    <div class="input-field col s8">
      <input id="Question" type="text" class="validate">
      <label for="Question">Quelle est la question?</label>
    </div>
    <div class="input-field col s8" id="The_Answers">
    </div>
    <div align=center>
      <button class="btn" type="submit">Envoyer<i class="material-icons">send</i></button>
    </div>
  </form>`;
};

function AddAnswer() {
	console.debug("AddAnswer()");
	const listrep = document.getElementById("list-BonneRep");
	const answers = document.getElementById("The_Answers");
	const nbAnswers = answers.childElementCount;
	const str1 = "nbr-";
	const str2 = str1.concat("", nbAnswers);
	const newClass = document.createElement("div");
	const newAnswer = document.createElement("INPUT");
	const html = `<option id=${str2}>La réponse ${nbAnswers}</option>`;
	listrep.innerHTML = listrep.innerHTML.concat(html);
	newAnswer.setAttribute("id", nbAnswers);
	newClass.appendChild(newAnswer);
	answers.appendChild(newClass);
}

function GetValueChampAnsw(nbAnswers, id) {
	const trueId = id.substr(4);
	const listAnsw = [];
	for (var i = 0; i < nbAnswers; i++) {
		let acc = {
			content: document.getElementById(i).value,
			proposition_id: i,
			correct: trueId == i,
		};
		listAnsw.push(acc);
	}
	return listAnsw;
}

function EditerQuizz() {
	const main = document.getElementById("id-all-quizzes-main");
	const html = state.UserQuizzes.map(
		(q) => `<option id="${q.quiz_id}" value="${q.quiz_id}">${q.title}</option>`
	);
	main.innerHTML = formulaireEditer(html);
	const form = document.forms["EditQuizz"];
	form.addEventListener("submit", (e) => {
		e.preventDefault();
		const answers = document.getElementById("The_Answers");
		const idselection = document.getElementById("list").selectedIndex;
		const IDquizz = document.getElementById("list").options[idselection];
		const Question = document.getElementById("Question").value;
		const nbAnswers = answers.childElementCount;
		const idselectionTrue = document.getElementById("list-BonneRep")
			.selectedIndex;
		const IDquizzSelect = document.getElementById("list-BonneRep").options[
			idselectionTrue
		];
		listRep = GetValueChampAnsw(nbAnswers, IDquizzSelect.id);
		data = {
			question_id: 0,
			sentence: Question,
			propositions: listRep,
		};
		postQuestion(data, IDquizz.id);
	});
}

const formulaireSuppQuizz = (html) => {
	return `<h4>Supprimer un Quizzz</h4>
  <form id="SuppQuiz">
  <div class="input-field col s8">
  <select id="list" class="browser-default">
    <option value="" disabled selected>${state.user.lastname}, veuillez selectionner le quizz à supprimer.</option>${html}
  </select>
  </div>
  </br><button class="btn" type="submit">
  <span>Supprimer</span><i class="material-icons">delete</i>
  </button></div>
  </form>`;
};

const SupprimerQuizz = () => {
	const main = document.getElementById("id-all-quizzes-main");
	const html = state.UserQuizzes.map(
		(q) => `<option id="${q.quiz_id}" value="${q.quiz_id}">${q.title}</option>`
	);
	main.innerHTML = formulaireSuppQuizz(html);
	const form = document.forms["SuppQuiz"];
	form.addEventListener("submit", (e) => {
		e.preventDefault();
		const idselection = document.getElementById("list").selectedIndex;
		const a = document.getElementById("list").options[idselection];
		deleteQuiz(a.id);
	});
};

// ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Permettre de choisir l'utilisateur avec lequel on se connecte et se délogguer le cas échéant
// ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// action à réaliser à la deconnection
const deconnection = () => {
	const UserAnswer = document.getElementById("id-my-answers-list");
	const main = document.getElementById("id-my-answers-main");
	main.innerHTML =
		"</br></br></br></br></br></br></br></br>Impossible d'afficher ceci car vous n'êtes pas connecté";
	UserAnswer.innerHTML = "";
	state.user = undefined;
	state.UserQuizzes = [];
	state.UserQuizzesID = [];
	state.xApiKey = "";
	return "sentiment_very_dissatisfied";
};

// action à réaliser pour la connection
const htmlConnect = (deco) => {
	const main = document.getElementById("id-my-answers-main");
	const main2 = document.getElementById("id-all-quizzes-main");
	main.innerHTML = "";
	main2.innerHTML = "";
	console.debug(`@htmlConnect()`);
	if (deco == true) {
		return `<h5 class="center-align">Voulez-vous vraiment vous déconnecter ${state.user.lastname}?</h5>`;
	} else
		return `<h5 class="center-align">Veuillez entrer votre XapiKey</h5>
  <form> 
    <div class="input-field">
      <input id="apiKey" type="text">
    </div>
  <form>`;
};

// le bouton pour la connection ou la deconnection
const renderUserBtn = () => {
	console.debug("@renderUserBtn()");
	const btn = document.getElementById("id-login");
	const modal = document.getElementById("id-modal-quizz-menu");
	const btnVert = document.getElementById("green");
	const btnRouge = document.getElementById("red");
	btn.classList.add("modal-trigger");
	btn.dataset.target = "id-modal-quizz-menu";
	if (state.user) btn.innerHTML = "mood";
	btn.onclick = () => {
		if (state.user) {
			modal.children[1].innerHTML = htmlConnect(true);
			btnRouge.innerHTML = "Déconnexion";
			btnVert.innerHTML = "Annuler";
			btnRouge.onclick = () => {
				btn.innerHTML = deconnection();
				renderQuizzes();
			};
		} else {
			modal.children[1].innerHTML = htmlConnect(false);
			btnRouge.innerHTML = "Annuler";
			btnVert.innerHTML = "Connection";
			btnVert.onclick = () => {
				state.xApiKey = document.getElementById("apiKey").value;
				getUser();
				renderQuizzes();
			};
		}
	};
};

// ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// RENDUS : mise en place du HTML dans le DOM et association des événemets: MES QUIZ
// ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// même principe que renderQuizzes() mais simplifié (moins de fonctionnalité associé)
function renderUserAnswers() {
	console.debug(`@renderUserAnswer()`);
	const QuizAnswers = document.getElementById("id-my-answers-list");
	const main = document.getElementById("id-my-answers-main");
	const html = state.UserAnswers.map(
		(q) =>
			`<li class="collection-item modal-trigger cyan lighten-5" data-quizzid="${q.quiz_id}">
      <h5>${q.title}</h5>
       ${q.description} <a class="chip">${q.owner_id}</a>      
      </li>`
	);

	QuizAnswers.innerHTML = `<ul class="collection">${html.join("")}`;

	const quizzes = document.querySelectorAll("#id-my-answers-list li");
	const prevBtn = document.getElementById("id-prev-quizzes");
	const nextBtn = document.getElementById("id-next-quizzes");

	function clickBtnPager() {
		getQuizzes(this.dataset.page);
	}
	if (prevBtn) prevBtn.onclick = clickBtnPager;
	if (nextBtn) nextBtn.onclick = clickBtnPager;

	function clickQuiz() {
		getUserAnswers();
		const quizzId = this.dataset.quizzid;

		console.debug(`@clickQuiz(${quizzId})`);

		state.currentQuizz = quizzId;

		return getDonneesQuizz(quizzId).then(() => {
			return renderCurrentQuizz(main);
		});
	}
	quizzes.forEach((q) => {
		q.onclick = clickQuiz;
	});
}
