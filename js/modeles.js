/* globals renderQuizzes renderUserBtn renderCurrentQuizz renderUserQuizzes renderUserAnswers */

// /////////////////////////////////////////////////////////////////////////////////////
// ETAT GLOBAL version modifié
// /////////////////////////////////////////////////////////////////////////////////////

// state est un objet global qui encapsule l'état de l'application
const state = {
	// la clef de l'utilisateur (STRING)
	xApiKey: "f0ddfe5e-e425-4850-b177-a7368669450e",

	// l'URL du serveur où accéder aux données (STRING)
	serverUrl: "https://lifap5.univ-lyon1.fr",

	// le quiz sélectionné
	currentQuizz: undefined,

	// la liste de tout les quizzes
	quizzes: [],

	// la liste des quizzes créé par l'USER connecté
	UserQuizzes: [],

	// la liste des ID des quizzes créé par l'USER connecté
	UserQuizzesID: [],

	// la liste des quizzes où l'USER connecté à deja repondu
	UserAnswers: [],

	// la liste des quizzes qui sont recherché
	research: [],

	// la liste des ID des quizzes qui sont recherché
	researchID: [],

	// les différents paramètres utiles pour le tri (OBJECT)
	tri: { limit: 10, order: "quiz_id", dir: "asc" },

	// retourne un headers utile pour les différents fetch (FONCTION)
	getHeaders() {
		const headers = new Headers();
		headers.set("X-API-KEY", this.xApiKey);
		headers.set("Accept", "application/json");
		headers.set("Content-Type", "application/json");
		return headers;
	},
};

// /////////////////////////////////////////////////////////////////////////////////////
// OUTILS génériques (version client de départ)
// /////////////////////////////////////////////////////////////////////////////////////

// un filtre simple pour récupérer les réponses HTTP - erreur
function filterHttpResponse(response) {
	return response
		.json()
		.then((data) => {
			if (response.status >= 400 && response.status < 600) {
				throw new Error(`${data.name}: ${data.message}`);
			}
			return data;
		})
		.catch((err) => {
			console.error(`Error on json: ${err}`);

			// retourne l'erreur convertie en string pour l'affichage des erreurs
			return err.toString();
		});
}

// /////////////////////////////////////////////////////////////////////////////////////
// DONNEES DE L'UTILISATEUR (version client de départ)
// /////////////////////////////////////////////////////////////////////////////////////

// mise-à-jour asynchrone de l'état avec les informations de l'utilisateur
// l'utilisateur est identifié via sa clef X-API-KEY lue dans l'état
// recupère les données de l'user (ses reponses, ses quizz)
// puis lance le rendu pour la connection
const getUser = () => {
	console.debug(`@getUser()`);
	const url = `${state.serverUrl}/users/whoami`;
	return fetch(url, { method: "GET", headers: state.getHeaders() })
		.then(filterHttpResponse)
		.then((data) => {
			// /!\ ICI L'ETAT EST MODIFIE /!\
			state.user = data;
			getUserQuizzes();
			getUserAnswers();
			return renderUserBtn();
		});
};

// /////////////////////////////////////////////////////////////////////////////////////
// DONNEES DES QUIZZES (version client de départ)
// /////////////////////////////////////////////////////////////////////////////////////

// mise-à-jour asynchrone de la liste des quizz
// la liste est trié grace à l'objet tri lue dans l'état
// et p est la page actuel par defaut 0
// on stock les données dans l'etat globale
// puis on lance le rendu de la liste des quizz
// (par defaut p=1 limit=10, order=quiz_id, dir=asc)
const getQuizzes = (p = 1) => {
	console.debug(`@getQuizzes(${p})`);
	const url = `${state.serverUrl}/quizzes/?page=${p}&limit=${state.tri.limit}&order=${state.tri.order}&dir=${state.tri.dir}`;
	return fetch(url, { method: "GET", headers: state.getHeaders() })
		.then(filterHttpResponse)
		.then((data) => {
			state.quizzes = data;
			return renderQuizzes();
		});
};

// /////////////////////////////////////////////////////////////////////////////////////
// DONNEES DES QUIZZES DE UTILISATEUR (new) recupere la liste des quizz de l'utilisateur
// /////////////////////////////////////////////////////////////////////////////////////

// mise-à-jour asynchrone de la liste des quizz de l'user
// on vérifie que l'user est bien connecté
// puis on stock dans l'etat globale la liste [Array] des quiz de l'user
// et la liste [Array] des id [Array]
const getUserQuizzes = () => {
	console.debug(`@getUserQuizzes()`);
	if (state.xApiKey != "") {
		const url = `${state.serverUrl}/users/quizzes`;
		return fetch(url, { method: "GET", headers: state.getHeaders() })
			.then(filterHttpResponse)
			.then((data) => {
				state.UserQuizzes = data;
				state.UserQuizzesID = state.UserQuizzes.map((r) => r.quiz_id);
			});
	}
};

// /////////////////////////////////////////////////////////////////////////////////////
// DONNEES DES QUIZZES (new) recupere les données d'un quizz
// /////////////////////////////////////////////////////////////////////////////////////

// retourne les informations sur les
// questions du quiz (sentence, question_id...)
const getQuestion = (quiz_id) => {
	console.debug(`@getQuestion(${quiz_id})`);
	const url = `${state.serverUrl}/quizzes/${quiz_id}`;
	return fetch(url, { method: "GET", headers: state.getHeaders() })
		.then(filterHttpResponse)
		.then((data) => {
			return data;
		});
};

// retourne les informations du quiz (titre, description...)
const getInfo = (quiz_id) => {
	console.debug(`@getinfo(${quiz_id})`);
	const url = `${state.serverUrl}/quizzes/${quiz_id}/questions`;
	return fetch(url, { method: "GET", headers: state.getHeaders() })
		.then(filterHttpResponse)
		.then((data) => {
			return data;
		});
};

// stock les données du quizz dans un objet
// on attend l'arrivée des donnees de getinfo et getQuestion
// puis on stock les donnes du quiz dans l'etat globale

async function getDonneesQuizz(quiz_id) {
	console.debug(`@getDonneesQuizz(${quiz_id})`);
	if (quiz_id != undefined) {
		const a = await getInfo(quiz_id);
		const b = await getQuestion(quiz_id);
		state.currentQuizzData = { questions: a, info: b };
		return state.currentQuizzData;
	}
}

// /////////////////////////////////////////////////////////////////////////////////////
// RECHERCHE QUIZZ
// /////////////////////////////////////////////////////////////////////////////////////

// retourne la liste des quizz qui corresponde à la recherche
// puis cette derniere dans l'etat globale

function searchQuiz(r) {
	console.debug(`@search(${r})`);
	const url = `${state.serverUrl}/search/?q=${r}`;
	return fetch(url, { method: "GET", headers: state.getHeaders() })
		.then(filterHttpResponse)
		.then((data) => {
			state.research = data;
		});
}

// Quand l'user tape une lettre ou clique on récupere la valeur du champ
// et on l'envoie dans le search on attend la reponse
// puis on stock les ids dans l'etat global et on relance le render
// pour afficher tout les quizz de la recherche en jaune
// state.researchID (plus facile pour evaluer la correspondance avec tout les quiz)
async function searchEngine(event) {
	const { value } = event.target;
	await searchQuiz(value);
	state.researchID = state.research.map((r) => r.quiz_id);
	renderQuizzes();
}

// quand on clique sur la croix pour fermer la recherche
// on réinitialise la recherche à zero et on relance le render
// pour afficher tout les quizz de la même couleur
const closeSearch = () => {
	moteurRecherche = document.getElementById("search");
	moteurRecherche.value = "";
	state.researchID = [];
	state.research = [];
	renderQuizzes();
};

// /////////////////////////////////////////////////////////////////////////////////////
// DONNEES DES QUIZZES DE UTILISATEUR (new) recupere les quizz où user à répondu
// /////////////////////////////////////////////////////////////////////////////////////

// recupère les quizz où l'utilisateur à deja répondu
// on stock les données dans l'etat globale
// puis on lance le rendu de la liste

const getUserAnswers = () => {
	console.debug(`@getUserAnswers()`);
	if (state.xApiKey != "") {
		const url = `${state.serverUrl}/users/answers`;
		return fetch(url, { method: "GET", headers: state.getHeaders() })
			.then(filterHttpResponse)
			.then((data) => {
				state.UserAnswers = data;
				return renderUserAnswers();
			});
	}
};
