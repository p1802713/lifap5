/* global M getUser getQuizzes state filterHttpResponse installWebSocket */

// envoie les réponses d'un quizz au serveur
function postAnswer(f, data) {
	console.debug(`@postAnswer(${(f, data)})`);
	for (const pair of f.entries()) {
		data[pair[0]] = pair[1];
		fetch(
			`${state.serverUrl}/quizzes/${state.currentQuizz}/questions/${
				pair[0]
			}/answers/${data[pair[0]]}`,
			{ method: "POST", headers: state.getHeaders() }
		)
			.then(filterHttpResponse)
			.then(getUserAnswers());
	}
	alert("Bien envoyé");
}

// envoie le nouveau quiz de l'user au serveur
function postQuizz(data) {
	console.debug(`@postQuizz(${data})`);
	const url = `${state.serverUrl}/quizzes/`;
	const body = JSON.stringify(data);
	fetch(url, { method: "POST", headers: state.getHeaders(), body })
		.then(filterHttpResponse)
		.then((r) => {
			try {
				r.split(":");
				alert(r);
			} catch {
				alert("Votre quizz est sympa");
				getQuizzes();
				getUserQuizzes();
			}
		});
}

// envoie le quizz modifier de l'user au serveur
function modifieQuizz(data, id) {
	console.debug(`@modifieQuizz(${(data, id)})`);
	const url = `${state.serverUrl}/quizzes/${id}`;
	const body = JSON.stringify(data);
	fetch(url, { method: "PUT", headers: state.getHeaders(), body })
		.then(filterHttpResponse)
		.then((r) => {
			try {
				r.split(":");
				alert(r);
			} catch {
				alert("Vos info du quizz sont maj");
				getQuizzes();
				getUserQuizzes();
			}
		});
}

// envoie la nouvelle question du quizz de l'user au serveur
function postQuestion(data, id) {
	console.debug(`@postQuestion(${(data, id)})`);
	const url = `${state.serverUrl}/quizzes/${id}/questions`;
	const body = JSON.stringify(data);
	fetch(url, { method: "POST", headers: state.getHeaders(), body })
		.then(filterHttpResponse)
		.then((r) => {
			try {
				r.split(":");
				alert(r);
			} catch {
				alert("Votre question est pertinante");
				getQuizzes();
				getUserQuizzes();
			}
		});
}

// envoie les id du quiz auquel appartient la question
// mais aussi l'ID de la question à supprimer au serveur
function deleteQuestion(Qid, id) {
	console.debug(`@deleteQuiz(${(Qid, id)})`);
	const url = `${state.serverUrl}/quizzes/${id}/questions/${Qid}`;
	fetch(url, { method: "DELETE", headers: state.getHeaders() })
		.then(filterHttpResponse)
		.then(alert("Bye bye le question"));
	getQuizzes();
	getUserQuizzes();
}

// envoie les donnees d'une question à mettre à jour avec les ids:
// id=>quiz | Qid=>question
function updateQuestion(data, id, Qid) {
	console.debug(`@updateQuestion(${(data, id, Qid)})`);
	const url = `${state.serverUrl}/quizzes/${id}/questions/${Qid}`;
	const body = JSON.stringify(data);
	fetch(url, { method: "PUT", headers: state.getHeaders(), body })
		.then(filterHttpResponse)
		.then((r) => {
			try {
				r.split(":");
				alert(r);
			} catch {
				alert("Votre question a etait maj");
				getQuizzes();
				getUserQuizzes();
			}
		});
}

// envoie l'id du quiz de l'user à supprimer au serveur
function deleteQuiz(id) {
	console.debug(`@deleteQuiz(${id})`);
	const url = `${state.serverUrl}/quizzes/${id}/`;
	fetch(url, { method: "DELETE", headers: state.getHeaders() })
		.then(filterHttpResponse)
		.then(alert("Bye bye le quizz"));
	getQuizzes();
	getUserQuizzes();
}

// //////////////////////////////////////////////////////////////////////////////
// PROGRAMME PRINCIPAL
// //////////////////////////////////////////////////////////////////////////////

function app() {
	console.debug(`@app()`);
	// ici, on lance en parallèle plusieurs actions
	return Promise.all([getUser(), getQuizzes()]).then(() =>
		console.debug(`@app(): OK`)
	);
}

// pour initialiser la bibliothèque Materialize
// https://materializecss.com/auto-init.html
M.AutoInit();

// lancement de l'application
app();

// pour installer le websocket
// sendMessage = installWebSocket(console.log);
